from src.cars import Car


def test_initial_state_of_a_car():
    car = Car('Toyota', 'black', 1991)
    assert car.brand == 'Toyota'
    assert car.colour == 'black'
    assert car.production_year == 1991
    assert car.mileage == 0


def test_drive_car_once():
    car = Car('Toyota', 'black', 1991)
    car.drive(100)
    assert car.mileage == 100


def test_drive_car_twice():
    car = Car('Toyota', 'black', 1991)
    car.drive(100)
    car.drive(100)
    assert car.mileage == 200
