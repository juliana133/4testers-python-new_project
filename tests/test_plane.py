from src.plane import Airplane


def test_new_plane_fly_distance_and_seats():
    plane1 = Airplane("Boeing 777", 550)
    assert plane1.total_distance_flown == 0
    assert plane1.taken_seats == 0


def test_plane_fly_distance():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly_distance(5000)
    assert plane1.total_distance_flown == 5000


def test_plane_fly_distance_in_two_travels():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly_distance(5000)
    plane1.fly_distance(3000)
    assert plane1.total_distance_flown == 8000


def test_service_is_not_required():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly_distance(9999)
    assert not plane1.is_service_required()


def test_service_is_required():
    plane1 = Airplane("Boeing 777", 550)
    plane1.fly_distance(10001)
    assert plane1.is_service_required()


def test_available_seats():
    plane1 = Airplane("Boeing", 200)
    plane1.board_passengers(180)
    assert plane1.get_available_seats() == 20


def test_overboard_plane():
    plane1 = Airplane("Boeing", 200)
    plane1.board_passengers(201)
    assert plane1.get_available_seats() == 0
